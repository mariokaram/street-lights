import sqlite from "sqlite";
import SQL from "sql-template-strings";

const initializeDatabase = async () => {
  const db = await sqlite.open("./mydb.sqlite");

  const createLight = async props => {
    const { era, metal, condition } = props;
    const result = await db.run(
      SQL`INSERT INTO Lights (era, metal, condition) VALUES (${era}, ${metal}, ${condition})`
    );
    const id = result.stmt.lastID;
    return id;
  };

  const deleteLight = async id => {
    const result = await db.run(SQL`DELETE FROM Lights WHERE id = ${id}`);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const updateLight = async (id, props) => {
    const { era, metal, condition } = props;
    const result = await db.run(
      SQL`UPDATE Lights SET 
      era=${era}, 
      metal= ${metal},
      condition=${condition},
      
      WHERE id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const getLight = async id => {
    const LightsList = await db.all(
      SQL`SELECT id, era,metal,condition FROM Lights WHERE id = ${id}`
    );
    const Light = LightsList[0];
    return Light;
  };

  const getLightsList = async orderBy => {
    let statement = `SELECT id, era,metal,condition FROM Lights`;
    switch (orderBy) {
      case "era":
        statement += ` ORDER BY era`;
        break;
      case "metal":
        statement += ` ORDER BY metal`;
        break;
      case "condition":
        statement += ` ORDER BY condition`;
        break;
      default:
        break;
    }
    const rows = await db.all(statement);
    return rows;
  };

  const controller = {
    createLight,
    deleteLight,
    updateLight,
    getLight,
    getLightsList
  };
  return controller;
};

export default initializeDatabase;
